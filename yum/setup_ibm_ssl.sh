#!/bin/sh
#
# global
#

PATH=/opt/freeware/bin:/QOpenSys/usr/bin:/QOpenSys/usr/sbin
LIBPATH=""
export PATH
export LIBPATH
OS400_BUNDLE="os400_bundle_v1"


function package_fixup_ibm_libs {
  cdhere=$(pwd)
  echo "x            0 *** start IBM i compatible AIX toolbox changes *** "
  echo "x            0 cd $cdhere"
  cd $cdhere

  echo "x            0 *** start IBM i untar $OS400_BUNDLE *** "
  echo "x            0 tar -xf $OS400_BUNDLE.tar"
  tar -xf $OS400_BUNDLE.tar
  echo "x            0 *** end IBM i untar $OS400_BUNDLE *** "
  echo "x            0 cd $cdhere"
  cd $cdhere

  echo "x            0 *** start IBM i aix lib stubs *** "
  echo "x            0 cd $OS400_BUNDLE/lib-patch"
  cd $OS400_BUNDLE/lib-patch
  ibm_libs=""
  ibm_libs="$ibm_libs libcfg.a libodm.a"
  for ibm_lib in $ibm_libs; do
    echo "x            0 cp $ibm_lib /QOpenSys/usr/lib/$ibm_lib"
    cp $ibm_lib /QOpenSys/usr/lib/$ibm_lib
  done
  echo "x            0 *** end IBM i aix lib stubs *** "
  echo "x            0 cd $cdhere"
  cd $cdhere

  echo "x            0 *** start IBM i ssl  *** "
  echo "x            0 cd $OS400_BUNDLE/lib-patch"
  cd $OS400_BUNDLE/lib-patch

  CNTSO=$(ar -t /QOpenSys/usr/lib/libssl.a | awk '{print $0 ".done"}' | grep -c 'libssl.so.done')
  if (($CNTSO==0)); then
    echo "x            0 cp libssl.so.1.0.1 libssl.so"
    cp libssl.so.1.0.1 libssl.so
    echo "x            0 ar -ruv /QOpenSys/usr/lib/libssl.a libssl.so"
    ar -ruv /QOpenSys/usr/lib/libssl.a libssl.so
    echo "x            0 ar -ruv /QOpenSys/usr/lib/libssl.a libssl.so.1.0.1"
    ar -ruv /QOpenSys/usr/lib/libssl.a libssl.so.1.0.1
  fi
  CNTSO=$(ar -t /QOpenSys/usr/lib/libssl.a | awk '{print $0 ".done"}' | grep -c 'libssl.so.done')
  if (($CNTSO==0)); then
    echo "x            0 *** IBM i - fatal error, unable to update /QOpenSys/usr/lib/libssl.a(libssl.so)"
    exit
  else
    echo "x            0 *** IBM i success /QOpenSys/usr/lib/libssl.a(libssl.so)"
  fi

  CNTSO=$(ar -t /QOpenSys/usr/lib/libcrypto.a | awk '{print $0 ".done"}' | grep -c 'libcrypto.so.done')
  if (($CNTSO==0)); then
    echo "x            0 cp libcrypto.so.1.0.1 libcrypto.so"
    cp libcrypto.so.1.0.1 libcrypto.so
    echo "x            0 ar -ruv /QOpenSys/usr/lib/libcrypto.a libcrypto.so"
    ar -ruv /QOpenSys/usr/lib/libcrypto.a libcrypto.so
    echo "x            0 ar -ruv /QOpenSys/usr/lib/libcrypto.a libcrypto.so.1.0.1"
    ar -ruv /QOpenSys/usr/lib/libcrypto.a libcrypto.so.1.0.1
  fi
  CNTSO=$(ar -t /QOpenSys/usr/lib/libcrypto.a | awk '{print $0 ".done"}' | grep -c 'libcrypto.so.done')
  if (($CNTSO==0)); then
    echo "x            0 *** IBM i - fatal error, unable to update /QOpenSys/usr/lib/libcrypto.a(libcrypto.so)"
    exit
  else
    echo "x            0 *** IBM i success /QOpenSys/usr/lib/libcrypto.a(libcrypto.so)"
  fi

  echo "x            0 *** end IBM i ssl *** "
  echo "x            0 cd $cdhere"
  cd $cdhere

  echo "x            0 *** end IBM i compatible AIX toolbox changes *** "
}

#
# main
#
if [ -d /QOpenSys/usr/bin ]
then
  package_fixup_ibm_libs
fi

