#!/bin/sh
#
# global
#

PATH=/opt/freeware/bin:/QOpenSys/usr/bin:/QOpenSys/usr/sbin
LIBPATH=""
export PATH
export LIBPATH
CREATEREPO_BUNDLE="createrepo_bundle_v1"
OS400_BUNDLE="os400_bundle_v1"

function package_fixup_createrepo {
  cdhere=$(pwd)
  echo "x            0 *** start IBM i compatible AIX toolbox changes *** "
  echo "x            0 cd $cdhere"
  cd $cdhere
  echo "x            0 *** end IBM i compatible AIX toolbox changes *** "

  echo "x            0 *** end createrepo install *** "

}

#
# setup RPM (standard)
#
function package_setup_createrepo {
  cdhere=$(pwd)
  echo "setup $CREATEREPO_BUNDLE ..."
  echo "x            0 tar -xf $CREATEREPO_BUNDLE.tar"
  tar -xf $CREATEREPO_BUNDLE.tar

  echo "x            0 *** start createrepo install *** "
  echo "x            0 cd $CREATEREPO_BUNDLE/aix_toolbox_rpms"
  cd $CREATEREPO_BUNDLE/aix_toolbox_rpms
  echo "x            0 rpm -hiv --replacepkgs --noscripts *.rpm"
  rpm -hiv --replacepkgs --noscripts *.rpm
  echo "x            0 cd $cdhere"
  cd $cdhere
}

function package_test_createrepo {
  echo "**********************"
  echo "createrepo --version"
  echo "**********************"
  echo "PATH=$PATH"
  echo "LIBPATH=$LIBPATH"
  createrepo --version
}

#
# main
#
if [ -d /QOpenSys/usr/bin ]
then
  package_setup_createrepo
  package_fixup_createrepo
  package_test_createrepo
fi

