#!/bin/sh
#
# global
#

PATH=/opt/freeware/bin:/QOpenSys/usr/bin:/QOpenSys/usr/sbin
LIBPATH=""
export PATH
export LIBPATH

#
# main
#
if [ -d /QOpenSys/usr/bin ]
then
  ./setup_ibm_ssl.sh
  CNTSO=$(ar -t /QOpenSys/usr/lib/libssl.a | awk '{print $0 ".done"}' | grep -c 'libssl.so.done')
  if (($CNTSO==0)); then
    echo "x            0 *** IBM i - fatal error, unable to update /QOpenSys/usr/lib/libssl.a(libssl.so)"
    exit
  fi
  ./setup_rpm.sh
  ./setup_yum.sh
  ./setup_createrepo.sh
fi
