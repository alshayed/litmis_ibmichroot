# <---- Experimental yum and hosting rpms---->
We are working an experimental version of yum installation. 
At this point, this project should be considered experiemental.
When the tag is removed the project is production.

#WARNING
*** Only new chroots have been tested to date. ***


*** =========================================== ***

*** status ***

*** =========================================== ***

Eventually, you may be able to install either aix or os400 rpms.
You will be able to control and restrict to fit your policies (OPS only, etc.). 
However, we have not tested all possibilities (need time). 
If you lack tolerance for experimentation, i suggest you
wait until common scenarios are documented.

#IBM i yum
This project is working with rpm+yum+createrepo to replace poor man's yum(ish) pkg_setup.sh and *.lst(s).

Project yum (setup_all.sh).
```
- rpm4 -- yum compatible rpm (setup_rpm.sh).
- yum -- yum rpm installer (setup_yum.sh).
- createrepo -- yum compatible hosting of rpms via Apache, local files, etc. (setup_createrepo.sh) 
```

# installation chroot (optional)
```
$ mkdir -p /QOpenSys/chroot_dir/home/profile/yum                    
$ ./chroot_setup.sh chroot_minimal.lst /QOpenSys/chroot_dir
-- enter chroot (as admin) --
$ chroot /QOpenSys/chroot_dir /QOpenSys/bin/ksh
$ chown -Rh profile /
$ exit
```

# ssh sign-on chroot (optional)
```
CHGUSRPRF USRPRF(PROFILE) LOCALE(*NONE) HOMEDIR(/QOpenSys/chroot_dir/./home/profile)
Note: '/./' is required for sign-on chroot
```

# download ibmichroot
https://bitbucket.org/litmis/ibmichroot (see Downloads)
```
$ unzip (download_name).zip
-- copy to IBM i --
$ cd (download_name)/yum
$ scp * admin@myibmi:/QOpenSys/chroot_dir/home/profile/yum

-- ftp to IBM i --
$ cd (download_name)/yum
$ ftp myibmi
> bin
> cd /QOpenSys/chroot_dir/home/profile/yum
> mput *
```

# prior to rpm installation (optional)
If you wish to remove AIX rpm install warnings:
```
warning: group system does not exist - using root
warning: group bin does not exist - using root
warning: user bin does not exist - using root
```
Create a group profile(s):
```
CRTUSRPRF USRPRF(SYSTEM) PASSWORD(*NONE) USRCLS(*SYSOPR) TEXT('AIX rpm group') SPCAUT(*USRCLS) GID(*GEN) AUT(*ALL)
CRTUSRPRF USRPRF(BIN) PASSWORD(*NONE) USRCLS(*SYSOPR) TEXT('AIX rpm group') SPCAUT(*USRCLS) GID(*GEN) AUT(*ALL)

Note: You may choose authorization settings.
```


# installation rpm, yum, createrepo (all)
Normal installation is setup_all.sh.

```
-- enter chroot as profile or ssh profile (optional chroot) --
$ chroot /QOpenSys/chroot_dir /QOpenSys/bin/ksh
$ export PATH=/opt/freeware/bin:/QOpenSys/usr/bin:/QOpenSys/usr/sbin
$ unset LIBPATH
$ cd ibmichroot/yum
$ ./setup_all.sh
-- setup shows progress --
```

# Use yum
```
$ chroot /QOpenSys/chroot_dir /QOpenSys/bin/ksh
$ export PATH=/opt/freeware/bin:/QOpenSys/usr/bin:/QOpenSys/usr/sbin
$ unset LIBPATH
$ yum list available
$ yum install db2util.noarch
$ yum list installed
$ yum list db2util.noarch
$ yum remove db2util.noarch
```


# Configuration
Normal installation setup_all.sh provides a machine compatible rpmrc.
Utility nature of installing rpms is controlled by rpmrc file.
```
/opt/freeware/lib/rpm/rpmrc

$ cd yum
$ setup_rpmrc.sh

Note: 
- setup_rpmrc.sh sets up rmprc for machine serial number.
- setup-rpm.sh also provides same service for intial install.
```

Utility yum sources for rpms is controled by yum.conf.
You will need to modify file for yum/rpm download sources.
The best way to understand rpm repo sources is to view samples (below).  
```
/opt/freeware/etc/yum/yum.conf
(yum.conf-os400-ifs default)

Samples:
$ cd yum/os400_bundle_v1/yum-conf
$ ls -1
yum.conf-aixtoolbox - aix toolbox yum repo
yum.conf-os400-aix-mix - aix and possible litmis repo mix
yum.conf-os400-apache_basic_auth - self host Apache with basic authentication repo
yum.conf-os400-ifs - self host local IFS files system repo
yum.conf-os400-litmis - possible litmis repo
yum.conf-os400-mix - ifs loacla and litmis repo mix

```

To create a rpm repo for default installation (yum.conf-os400-ifs), 
find a few rpms (see other README files),
then use createrepo tool.
```
What is in our default yum conf?
$ grep baseurl /opt/freeware/etc/yum/yum.conf
baseurl=file:///QOpenSys/opt/freeware/src/packages/RPMS/noarch/

ok, make a rpm repo ...
$ cd /QOpenSys/opt/freeware/src/packages/RPMS/noarch/
$ ls
db2util-1.0.6.beta-1.os400.noarch.rpm  rsync-3.1.2-1.os400.noarch.rpm
$ createrepo .
Spawning worker 0 with 2 pkgs
:
Sqlite DBs complete
$ ls
db2util-1.0.6.beta-1.os400.noarch.rpm  repodata  rsync-3.1.2-1.os400.noarch.rpm

ok, what can yum install from ifs repo???
$ yum list available     
Config time: 0.027
:
Available Packages
db2util.noarch 1.0.6.beta-1 os400_file
rsync.noarch   3.1.2-1      os400_file

ok, yum install ...
$ yum install db2util.noarch
$ yum install rsync.noarch

No, changed my mind, remove ...
$ yum remove db2util.noarch
$ yum remove rsync.noarch

```

# Optional partial installation partial (manual process)
Normal installation is setup_all.sh.
However, install/re-install is possible using setup scripts. 

0) IBM i check/fix ssl (manditory)
```
-- enter chroot as profile or ssh profile (optional chroot) --
$ chroot /QOpenSys/chroot_dir /QOpenSys/bin/ksh
$ export PATH=/opt/freeware/bin:/QOpenSys/usr/bin:/QOpenSys/usr/sbin
$ unset LIBPATH
$ cd ibmichroot/yum
$ ./setup_ibm_ssl.sh

Note:
- setup_ibm_ssl.sh appends libssl.so/libcrypto.so to libssl.a/libcrypto.a used by aix toolbox
- when IBM i PTF updated, script will detect and no longer run
- you need authority to change libssl.a/libcrypto.a or setup will halt/fail.
```

1) RPM 4.9 (manditory)
```
-- enter chroot as profile or ssh profile (optional chroot) --
$ chroot /QOpenSys/chroot_dir /QOpenSys/bin/ksh
$ export PATH=/opt/freeware/bin:/QOpenSys/usr/bin:/QOpenSys/usr/sbin
$ unset LIBPATH
$ cd ibmichroot/yum
$ ./setup_rpm.sh
$ rpm --version
RPM version 4.9.1.3
```

2) install yum (suggested)
```
-- enter chroot as profile or ssh profile (optional chroot) --
$ chroot /QOpenSys/chroot_dir /QOpenSys/bin/ksh
$ export PATH=/opt/freeware/bin:/QOpenSys/usr/bin:/QOpenSys/usr/sbin
$ unset LIBPATH
$ cd ibmichroot/yum
$ ./setup_yum.sh
$ yum --version
3.4.3

Note:
- setup_yum.sh patches yum to allow non-qsecofr profiles to use yum (yumcommands.py/yumupd.py)
```

3) install createrepo (optional)
```
-- enter chroot as profile or ssh profile (optional chroot) --
$ chroot /QOpenSys/chroot_dir /QOpenSys/bin/ksh
$ export PATH=/opt/freeware/bin:/QOpenSys/usr/bin:/QOpenSys/usr/sbin
$ unset LIBPATH
$ cd ibmichroot/yum
$ ./setup_createrepo.sh
$ createrepo --version
createrepo 0.9.9
```


