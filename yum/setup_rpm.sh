#!/bin/sh
#
# global
#

PATH=/opt/freeware/bin:/QOpenSys/usr/bin:/QOpenSys/usr/sbin
LIBPATH=""
export PATH
export LIBPATH
RPM_BUNDLE="rpm_bundle_v1"
OS400_BUNDLE="os400_bundle_v1"
RPM_RTE="rpm.rte.4.9.1.3"


function package_fixup_rpm {
  cdhere=$(pwd)
  echo "x            0 *** start IBM i compatible AIX toolbox changes *** "
  echo "x            0 cd $cdhere"
  cd $cdhere

  echo "x            0 *** start IBM i /opt /var links *** "
  echo "x            0 mkdir -p /QOpenSys/var/tmp"
  mkdir -p /QOpenSys/var/tmp
  echo "x            0 mkdir -p /QOpenSys/var/log"
  mkdir -p /QOpenSys/var/log
  echo "x            0 mkdir -p /QOpenSys/var/lib/rpm/Packages"
  mkdir -p /QOpenSys/var/lib/rpm/Packages
  echo "x            0 ln -sf /QOpenSys/var /var"
  ln -sf /QOpenSys/var /var
  echo "x            0 ln -sf /QOpenSys/opt /opt"
  ln -sf /QOpenSys/opt /opt
  echo "x            0 ln -sf /QOpenSys/opt /QOpenSys/var/opt"
  ln -sf /QOpenSys/opt /QOpenSys/var/opt
  echo "x            0 *** end IBM i IBM i /opt /var links *** "
  echo "x            0 cd $cdhere"
  cd $cdhere

  echo "x            0 *** start IBM i untar $OS400_BUNDLE *** "
  echo "x            0 tar -xf $OS400_BUNDLE.tar"
  tar -xf $OS400_BUNDLE.tar
  echo "x            0 *** end IBM i untar $OS400_BUNDLE *** "
  echo "x            0 cd $cdhere"
  cd $cdhere

  echo "x            0 *** start IBM i rpm conf *** "
  echo "x            0 cd $OS400_BUNDLE/rpm-conf"
  cd $OS400_BUNDLE/rpm-conf
  unamem=$(uname -m)
  ureplace="s/REPLACEUNAME/$unamem/"
  echo "x            0 sed $ureplace rpmrc-os400 > rpmrc-this-machine"
  sed "$ureplace" rpmrc-os400 > rpmrc-this-machine
  echo "x            0 cp rpmrc-this-machine /opt/freeware/lib/rpm/rpmrc"
  cp rpmrc-this-machine /opt/freeware/lib/rpm/rpmrc
  echo "x            0 *** end IBM i rpm conf *** "
  echo "x            0 cd $cdhere"
  cd $cdhere

  echo "x            0 *** start /opt/freeware/lib links *** "
  echo "x            0 cd /opt/freeware/lib"
  cd /opt/freeware/lib
  echo "x            0 ln -sf libdb-4.8.so libdb.so"
  ln -sf libdb-4.8.so libdb.so
  echo "x            0 ln -sf libpopt.so.0 libpopt.so.0.0.0"
  ln -sf libpopt.so.0 libpopt.so.0.0.0
  echo "x            0 ln -sf libpopt.so.0 libpopt.so"
  ln -sf libpopt.so.0 libpopt.so
  echo "x            0 ln -sf librpm.so.2.0.3 librpm.so.0.0.0"
  ln -sf librpm.so.2.0.3 librpm.so.0.0.0
  echo "x            0 ln -sf librpm.so.2.0.3 librpm.so"
  ln -sf librpm.so.2.0.3 librpm.so
  echo "x            0 ln -sf librpmbuild.so.2.0.1 librpmbuild.so.0.0.0"
  ln -sf librpmbuild.so.2.0.1 librpmbuild.so.0.0.0
  echo "x            0 ln -sf librpmbuild.so.2.0.1 librpmbuild.so"
  ln -sf librpmbuild.so.2.0.1 librpmbuild.so
  echo "x            0 ln -sf librpmio.so.2.0.1 librpmio.so"
  ln -sf librpmio.so.2.0.1 librpmio.so
  echo "x            0 ln -sf librpmsign.so.0.0.1 librpmsign.so"
  ln -sf librpmsign.so.0.0.1 librpmsign.so
  echo "x            0 *** end /opt/freeware/lib links *** "
  echo "x            0 cd $cdhere"
  cd $cdhere

  echo "x            0 *** end rpm os400 provides *** "
  echo "x            0 cd $RPM_BUNDLE"
  cd $RPM_BUNDLE
  echo "x            0 rpm -hiv --replacepkgs --noscripts *.rpm"
  rpm -hiv --replacepkgs --noscripts *.rpm
  echo "x            0 *** end rpm os400 provides *** "
  echo "x            0 cd $cdhere"
  cd $cdhere

  echo "x            0 *** end IBM i compatible AIX toolbox changes *** "

  echo "x            0 *** end rpm install *** "
}

#
# setup RPM (standard)
#
function package_setup_rpm {
  cdhere=$(pwd)
  echo "setup $RPM_BUNDLE ..."
  echo "x            0 tar -xf $RPM_BUNDLE.tar"
  tar -xf $RPM_BUNDLE.tar

  echo "x            0 *** start rpm install *** "
  echo "x            0 cd $RPM_BUNDLE"
  cd $RPM_BUNDLE
  echo "setup $RPM_RTE ..."
  restore -xvqf $RPM_RTE
  echo "x            0 mkdir /QOpenSys/opt"
  mkdir /QOpenSys/opt
  echo "x            0 cp -R usr/opt/* /QOpenSys/opt/."
  cp -R usr/opt/* /QOpenSys/opt/.
  echo "x            0 rm -R usr"
  rm -R usr
  echo "x            0 rm lpp_name"
  rm lpp_name
  echo "x            0 cd $cdhere"
  cd $cdhere
}

function package_test_rpm {
  echo "**********************"
  echo "rpm --version"
  echo "**********************"
  echo "PATH=$PATH"
  echo "LIBPATH=$LIBPATH"
  rpm --version
}

#
# main
#
if [ -d /QOpenSys/usr/bin ]
then
  package_setup_rpm
  package_fixup_rpm
  package_test_rpm
fi

