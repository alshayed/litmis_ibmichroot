# simple create rpm and local file system repository
This simple example creates a rpm from existing binaries on my IBM i. 

standard install yum (setup_all.sh)
```
$ cat /opt/freeware/etc/yum/yum.conf
[main]
cachedir=/var/cache/yum
keepcache=0
debuglevel=10
logfile=/var/log/yum.log
exactarch=1
obsoletes=1

[os400_file]
name=IBM i generic repository
baseurl=file:///QOpenSys/opt/freeware/src/packages/RPMS/noarch/
enabled=1
gpgcheck=0

```

0) wget db2util binaries from yips site
```
$ export PATH=/opt/freeware/bin:/QOpenSys/usr/bin:/QOpenSys/usr/sbin
$ unset LIBPATH
$ cd /opt/freeware/src/packages/SOURCES
$ wget http://yips.idevcloud.com/wiki/uploads/Databases/db2util-1.0.6.beta.zip
converted 'http://yips.idevcloud.com/wiki/uploads/Databases/db2util-1.0.6.beta.zip' (ISO8859-1) -> 'http://yips.idevcloud.com/wiki/uploads/Databases/db2util-1.0.6.beta.zip' (UTF-8)
--2016-09-28 15:10:01--  http://yips.idevcloud.com/wiki/uploads/Databases/db2util-1.0.6.beta.zip
Resolving yips.idevcloud.com (yips.idevcloud.com)... 65.183.160.36
Connecting to yips.idevcloud.com (yips.idevcloud.com)|65.183.160.36|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 120780 (118K) [multipart/x-zip]
Saving to: 'db2util-1.0.6.beta.zip'

db2util-1.0.6.beta.zip                        100%[===================================================================================================>] 117.95K   723KB/s   in 0.2s   

2016-09-28 15:10:03 (723 KB/s) - 'db2util-1.0.6.beta.zip' saved [120780/120780]

$ unzip db2util-1.0.6.beta.zip 
Archive:  db2util-1.0.6.beta.zip
   creating: db2util-1.0.6.beta/
  inflating: db2util-1.0.6.beta/db2util  
  inflating: db2util-1.0.6.beta/db2fcgi  
$ 
```

1) create rpm spec file
```
$ export PATH=/opt/freeware/bin:/QOpenSys/usr/bin:/QOpenSys/usr/sbin
$ unset LIBPATH
$ cd /opt/freeware/src/packages/SPECS
$ cat db2util-1.0.6.beta.spec 
#
# Example spec file for db2util
#
# rpmbuild -ba --target=noarch db2util-1.0.6.beta.spec
#
Summary: A DB2 command line utility.
Name: db2util
Version: 1.0.6.beta
Release: 1
License: MIT
Source: http://yips.idevcloud.com/wiki/uploads/Databases/db2util-%{version}.zip
URL: https://bitbucket.org/litmis/db2util
BuildRoot: /var/tmp/db2util

%build

%install
mkdir -p /var/tmp/db2util-1.0.6.beta-1.ppc/QOpenSys/usr/bin/
cd /opt/freeware/src/packages/SOURCES/db2util-1.0.6.beta
cp * /var/tmp/db2util-1.0.6.beta-1.ppc/QOpenSys/usr/bin/.

%clean

%files
%defattr(-,root,system)
/QOpenSys/usr/bin/db2util
/QOpenSys/usr/bin/db2fcgi

%description
It slices!  It dices!  It's a DB2 utility.
```

2) create rpm based on spec file
```
$ rpmbuild -ba --target=noarch db2util-1.0.6.beta.spec
Building target platforms: noarch
Building for target noarch
Executing(%build): /bin/sh -e /var/tmp/rpm-tmp.3pbHma
+ umask 022
+ cd /opt/freeware/src/packages/BUILD
+ exit 0
Executing(%install): /bin/sh -e /var/tmp/rpm-tmp.3xbHmb
+ umask 022
+ cd /opt/freeware/src/packages/BUILD
+ mkdir -p /var/tmp/db2util-1.0.6.beta-1.ppc/QOpenSys/usr/bin/
+ cd /opt/freeware/src/packages/SOURCES/db2util-1.0.6.beta
+ cp db2fcgi db2util /var/tmp/db2util-1.0.6.beta-1.ppc/QOpenSys/usr/bin/.
+ exit 0
Processing files: db2util-1.0.6.beta-1.noarch
Requires(rpmlib): rpmlib(CompressedFileNames) <= 3.0.4-1 rpmlib(PayloadFilesHavePrefix) <= 4.0-1
Wrote: /opt/freeware/src/packages/SRPMS/db2util-1.0.6.beta-1.src.rpm
Wrote: /opt/freeware/src/packages/RPMS/noarch/db2util-1.0.6.beta-1.os400.noarch.rpm
Executing(%clean): /bin/sh -e /var/tmp/rpm-tmp.3XbHme
+ umask 022
+ cd /opt/freeware/src/packages/BUILD
+ exit 0
$ ls ../RPMS/noarch/.          
db2util-1.0.6.beta-1.os400.noarch.rpm
```

3) create a rpm repository
```
$ createrepo ../RPMS/noarch/.
Spawning worker 0 with 1 pkgs
Workers Finished
Saving Primary metadata
Saving file lists metadata
Saving other metadata
Generating sqlite DBs
Sqlite DBs complete
```

4) yum list available rpms in repository
```
$ yum list available
Config time: 0.027
Yum Version: 3.4.3
COMMAND: yum list available 
Installroot: /
Ext Commands:

   available
Setting up Package Sacks
os400_file                                                                                                                                                       | 2.9 kB  00:00:00     
os400_file/primary_db                                                                                                                                            | 1.8 kB  00:00:00     
pkgsack time: 0.056
Reading Local RPMDB
rpmdb time: 0.000
Available Packages
db2util.noarch                                                                          1.0.6.beta-1                                                                          os400_file
$ db2util
bash: db2util: command not found
```

step 5) yum install RPM from repository
```
$ yum install db2util.noarch
Config time: 0.027
Yum Version: 3.4.3
COMMAND: yum install db2util.noarch 
Installroot: /
Ext Commands:

   db2util.noarch
Setting up Package Sacks
os400_file                                                                                                                                                       | 2.9 kB  00:00:00     
os400_file/primary_db                                                                                                                                            | 1.8 kB  00:00:00     
pkgsack time: 0.077
Reading Local RPMDB
rpmdb time: 0.001
Setting up Install Process
Obs Init time: 0.000
Resolving Dependencies
--> Running transaction check
---> Package db2util.noarch 0:1.0.6.beta-1 will be installed
Checking deps for db2util.noarch 0:1.0.6.beta-1 - u
--> Finished Dependency Resolution
Dependency Process ending
Depsolve time: 0.103

Dependencies Resolved

========================================================================================================================================================================================
 Package                                   Arch                                     Version                                          Repository                                    Size
========================================================================================================================================================================================
Installing:
 db2util                                   noarch                                   1.0.6.beta-1                                     os400_file                                   117 k

Transaction Summary
========================================================================================================================================================================================
Install       1 Package

Total download size: 117 k
Installed size: 423 k
Is this ok [y/N]: y
Downloading Packages:
Member: db2util.noarch 0:1.0.6.beta-1 - u
Adding Package db2util-1.0.6.beta-1.noarch in mode u
Running Transaction Check
Transaction Check time: 0.001
Running Transaction Test
Transaction Test Succeeded
Transaction Test time: 0.007
Running Transaction
  Installing : db2util-1.0.6.beta-1.noarch                                                                                                                                          1/1 
VerifyTransaction time: 0.073
Transaction time: 0.263

Installed:
  db2util.noarch 0:1.0.6.beta-1                                                                                                                                                         

Complete!
$ db2util
Syntax: db2util 'sql statement' [-h -xc [xmlservice lib] -o [json|comma|space] -p parm1 parm2 ...]
$ db2util "select * from QIWS/QCUSTCDT where LSTNAM='Jones' or LSTNAM='Vine'"
"839283","Jones   ","B D","21B NW 135 St","Clay  ","NY","13041","400","1","100.00",".00"
"392859","Vine    ","S S","PO Box 79    ","Broton","VT","5046","700","1","439.00",".00"

Note: 
If using db2util in chroot must include PASE nls files: 
$ cd /QOpenSys/QIBM/ProdData/OPS/GCC
$./chroot_setup.sh chroot_nls.lst /QOpenSys/chroot_dir

If PASE nls files missing, expect error like:
Cannot convert between code set  (CCSID 819) and code set  (CCSID 37)
```


6) list installed rpm
```
$ yum list db2util.noarch
Config time: 0.027
Yum Version: 3.4.3
COMMAND: yum list db2util.noarch 
Installroot: /
Ext Commands:

   db2util.noarch
Setting up Package Sacks
os400_file                                                                                                                                                       | 2.9 kB  00:00:00     
os400_file/primary_db                                                                                                                                            | 1.8 kB  00:00:00     
pkgsack time: 0.079
Reading Local RPMDB
rpmdb time: 0.000
Installed Packages
db2util.noarch                                                                         1.0.6.beta-1                                                                          @os400_file
```

7) yum remove installed rpm
``` 
$ yum remove db2util.noarch                                                  
Config time: 0.027
Yum Version: 3.4.3
COMMAND: yum remove db2util.noarch 
Installroot: /
Ext Commands:

   db2util.noarch
Reading Local RPMDB
rpmdb time: 0.001
Setting up Remove Process
Resolving Dependencies
--> Running transaction check
---> Package db2util.noarch 0:1.0.6.beta-1 will be erased
Checking deps for db2util.noarch 0:1.0.6.beta-1 - e
--> Finished Dependency Resolution
Dependency Process ending
Depsolve time: 0.106

Dependencies Resolved

========================================================================================================================================================================================
 Package                                   Arch                                     Version                                         Repository                                     Size
========================================================================================================================================================================================
Removing:
 db2util                                   noarch                                   1.0.6.beta-1                                    @os400_file                                   423 k

Transaction Summary
========================================================================================================================================================================================
Remove        1 Package

Installed size: 423 k
Is this ok [y/N]: y
Downloading Packages:
Member: db2util.noarch 0:1.0.6.beta-1 - e
Removing Package db2util-1.0.6.beta-1.noarch
Running Transaction Check
Transaction Check time: 0.001
Running Transaction Test
Transaction Test Succeeded
Transaction Test time: 0.009
Running Transaction
  Erasing    : db2util-1.0.6.beta-1.noarch                                                                                                                                          1/1 
VerifyTransaction time: 0.054
Transaction time: 0.211

Removed:
  db2util.noarch 0:1.0.6.beta-1                                                                                                                                                         

Complete!
$ db2util
bash: /QOpenSys/usr/bin/db2util: No such file or directory
```
