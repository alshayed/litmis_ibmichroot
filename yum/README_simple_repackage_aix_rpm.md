# simple repackage aix toolbox rpm to os400 rpm
This simple example repackages a aix toolbox rpm on my IBM i. 

standard install yum (setup_all.sh)
```
$ cat /opt/freeware/etc/yum/yum.conf
[main]
cachedir=/var/cache/yum
keepcache=0
debuglevel=10
logfile=/var/log/yum.log
exactarch=1
obsoletes=1

[os400_file]
name=IBM i generic repository
baseurl=file:///QOpenSys/opt/freeware/src/packages/RPMS/noarch/
enabled=1
gpgcheck=0

```

0) wget rsync rpm from aix toolbox
```
$ cd /opt/freeware/src/packages/SOURCES
$ mkdir rsync
$ cd rsync  
$ wget ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/rsync/rsync-3.1.2-1.aix6.1.ppc.rpm
converted 'ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/rsync/rsync-3.1.2-1.aix6.1.ppc.rpm' (ISO8859-1) -> 'ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/rsync/rsync-3.1.2-1.aix6.1.ppc.rpm' (UTF-8)
--2016-09-28 15:29:20--  ftp://ftp.software.ibm.com/aix/freeSoftware/aixtoolbox/RPMS/ppc/rsync/rsync-3.1.2-1.aix6.1.ppc.rpm
           => 'rsync-3.1.2-1.aix6.1.ppc.rpm'
Resolving ftp.software.ibm.com (ftp.software.ibm.com)... 9.17.248.40
Connecting to ftp.software.ibm.com (ftp.software.ibm.com)|9.17.248.40|:21... connected.
Logging in as anonymous ... Logged in!
==> SYST ... done.    ==> PWD ... done.
==> TYPE I ... done.  ==> CWD (1) /aix/freeSoftware/aixtoolbox/RPMS/ppc/rsync ... done.
==> SIZE rsync-3.1.2-1.aix6.1.ppc.rpm ... 314516
==> PASV ... done.    ==> RETR rsync-3.1.2-1.aix6.1.ppc.rpm ... done.
Length: 314516 (307K) (unauthoritative)

rsync-3.1.2-1.aix6.1.ppc.rpm                  100%[===================================================================================================>] 307.14K  1.08MB/s   in 0.3s   

2016-09-28 15:29:20 (1.08 MB/s) - 'rsync-3.1.2-1.aix6.1.ppc.rpm' saved [314516]
```

1) install aix toolbox rpm (must be on system)
```
$ rpm -hiv rsync-3.1.2-1.aix6.1.ppc.rpm                                                                  
Preparing...                ########################################### [100%]
   1:rsync                  ########################################### [100%]
```

2) os400repackage aix rpm into os400 rpm spec
``` 
$ cd ..
$ os400repackage rsync 

---Directory ... rsync---
Found ... rsync-3.1.2-1.aix6.1.ppc.rpm
Reading ... rsync-3.1.2-1.aix6.1.ppc.rpm
/opt/freeware/bin/rsync ... ok
/opt/freeware/doc/rsync-3.1.2 ... ok
/opt/freeware/doc/rsync-3.1.2/COPYING ... ok
/opt/freeware/doc/rsync-3.1.2/README ... ok
/opt/freeware/doc/rsync-3.1.2/tech_report.tex ... ok
/opt/freeware/man/man1/rsync.1 ... ok
/opt/freeware/man/man5/rsyncd.conf.5 ... ok
/usr/bin/rsync ... ok
Writing ... ./os400_SPECS/rsync-3.1.2-1.os400.noarch.spec
$ ls      
os400_SPECS             rsync
$ cp os400_SPECS/rsync-3.1.2-1.os400.noarch.spec ../SPECS/.
$ cd ../SPECS/
$ cat rsync-3.1.2-1.os400.noarch.spec 
#
# rpmbuild -ba --target=noarch rsync-3.1.2-1.os400.noarch.spec
#
Summary: A program for synchronizing files over a network.
Name: rsync
Version: 3.1.2
Release: 1
License: GPLv3+
URL: http://rsync.samba.org
Group: Applications/Internet
BuildRoot: /var/tmp/rsync

%description
A program for synchronizing files over a network.
(repackage rsync-3.1.2-1.aix6.1.ppc.rpm from rsync).

%build

%clean


%install
rm -r /var/tmp/*
mkdir -p /var/tmp/rsync-3.1.2-1.ppc/opt/freeware/bin
mkdir -p /var/tmp/rsync-3.1.2-1.ppc/opt/freeware/doc/rsync-3.1.2
mkdir -p /var/tmp/rsync-3.1.2-1.ppc/opt/freeware/man/man1
mkdir -p /var/tmp/rsync-3.1.2-1.ppc/opt/freeware/man/man5
mkdir -p /var/tmp/rsync-3.1.2-1.ppc/usr/bin
cp /opt/freeware/bin/rsync /var/tmp/rsync-3.1.2-1.ppc/opt/freeware/bin/rsync
cp /opt/freeware/doc/rsync-3.1.2/COPYING /var/tmp/rsync-3.1.2-1.ppc/opt/freeware/doc/rsync-3.1.2/COPYING
cp /opt/freeware/doc/rsync-3.1.2/README /var/tmp/rsync-3.1.2-1.ppc/opt/freeware/doc/rsync-3.1.2/README
cp /opt/freeware/doc/rsync-3.1.2/tech_report.tex /var/tmp/rsync-3.1.2-1.ppc/opt/freeware/doc/rsync-3.1.2/tech_report.tex
cp /opt/freeware/man/man1/rsync.1 /var/tmp/rsync-3.1.2-1.ppc/opt/freeware/man/man1/rsync.1
cp /opt/freeware/man/man5/rsyncd.conf.5 /var/tmp/rsync-3.1.2-1.ppc/opt/freeware/man/man5/rsyncd.conf.5
cd /var/tmp/rsync-3.1.2-1.ppc/usr/bin
ln -sf ../../opt/freeware/bin/rsync rsync
chown -R -P qsys /var/tmp/rsync-3.1.2-1.ppc
%files
%defattr(-,root,system)
/opt/freeware/bin/rsync
/opt/freeware/doc/rsync-3.1.2/COPYING
/opt/freeware/doc/rsync-3.1.2/README
/opt/freeware/doc/rsync-3.1.2/tech_report.tex
/opt/freeware/man/man1/rsync.1
/opt/freeware/man/man5/rsyncd.conf.5
/usr/bin/rsync
```

3) create rpm based on spec file
```
$  rpmbuild -ba --target=noarch rsync-3.1.2-1.os400.noarch.spec
Building target platforms: noarch
Building for target noarch
Executing(%build): /bin/sh -e /var/tmp/rpm-tmp.p-bWMa
+ umask 022
+ cd /opt/freeware/src/packages/BUILD
+ exit 0
Executing(%install): /bin/sh -e /var/tmp/rpm-tmp.qpbWMb
+ umask 022
+ cd /opt/freeware/src/packages/BUILD
+ rm -r /var/tmp/db2util-1.0.6.beta-1.ppc /var/tmp/rpm-tmp.l0bTIb /var/tmp/rpm-tmp.qpbWMb /var/tmp/yum-yum-1gq6zP /var/tmp/yum-yum-2KxZ1F /var/tmp/yum-yum-34wrtK /var/tmp/yum-yum-5C3C6m /var/tmp/yum-yum-65Gt1W /var/tmp/yum-yum-90bpaq /var/tmp/yum-yum-BAnrDP /var/tmp/yum-yum-ECU0h5 /var/tmp/yum-yum-JGX00K /var/tmp/yum-yum-LghayZ /var/tmp/yum-yum-Of8E3c /var/tmp/yum-yum-PSZXVS /var/tmp/yum-yum-PnXB_0 /var/tmp/yum-yum-QznQ0A /var/tmp/yum-yum-RvFC56 /var/tmp/yum-yum-SBPfhS /var/tmp/yum-yum-TPsEob /var/tmp/yum-yum-ULlZi9 /var/tmp/yum-yum-XMW1mi /var/tmp/yum-yum-aKzxtH /var/tmp/yum-yum-fcHKUo /var/tmp/yum-yum-fp0qyI /var/tmp/yum-yum-hIpL2_ /var/tmp/yum-yum-kCaCSK /var/tmp/yum-yum-lBPhlg /var/tmp/yum-yum-lUEiii /var/tmp/yum-yum-njOTCX /var/tmp/yum-yum-q158ux /var/tmp/yum-yum-qA8Y2M /var/tmp/yum-yum-qeMg6J /var/tmp/yum-yum-tVJapB /var/tmp/yum-yum-urHx2B /var/tmp/zz-os400-provides.ppc
+ mkdir -p /var/tmp/rsync-3.1.2-1.ppc/opt/freeware/bin
+ mkdir -p /var/tmp/rsync-3.1.2-1.ppc/opt/freeware/doc/rsync-3.1.2
+ mkdir -p /var/tmp/rsync-3.1.2-1.ppc/opt/freeware/man/man1
+ mkdir -p /var/tmp/rsync-3.1.2-1.ppc/opt/freeware/man/man5
+ mkdir -p /var/tmp/rsync-3.1.2-1.ppc/usr/bin
+ cp /opt/freeware/bin/rsync /var/tmp/rsync-3.1.2-1.ppc/opt/freeware/bin/rsync
+ cp /opt/freeware/doc/rsync-3.1.2/COPYING /var/tmp/rsync-3.1.2-1.ppc/opt/freeware/doc/rsync-3.1.2/COPYING
+ cp /opt/freeware/doc/rsync-3.1.2/README /var/tmp/rsync-3.1.2-1.ppc/opt/freeware/doc/rsync-3.1.2/README
+ cp /opt/freeware/doc/rsync-3.1.2/tech_report.tex /var/tmp/rsync-3.1.2-1.ppc/opt/freeware/doc/rsync-3.1.2/tech_report.tex
+ cp /opt/freeware/man/man1/rsync.1 /var/tmp/rsync-3.1.2-1.ppc/opt/freeware/man/man1/rsync.1
+ cp /opt/freeware/man/man5/rsyncd.conf.5 /var/tmp/rsync-3.1.2-1.ppc/opt/freeware/man/man5/rsyncd.conf.5
+ cd /var/tmp/rsync-3.1.2-1.ppc/usr/bin
+ ln -sf ../../opt/freeware/bin/rsync rsync
+ chown -R -P qsys /var/tmp/rsync-3.1.2-1.ppc
+ exit 0
Processing files: rsync-3.1.2-1.noarch
Requires(rpmlib): rpmlib(CompressedFileNames) <= 3.0.4-1 rpmlib(PayloadFilesHavePrefix) <= 4.0-1
Wrote: /opt/freeware/src/packages/SRPMS/rsync-3.1.2-1.src.rpm
Wrote: /opt/freeware/src/packages/RPMS/noarch/rsync-3.1.2-1.os400.noarch.rpm
Executing(%clean): /bin/sh -e /var/tmp/rpm-tmp.vLbWMe
+ umask 022
+ cd /opt/freeware/src/packages/BUILD
+ exit 0
```

4) create a rpm repository
```
$ ls ../RPMS/noarch/
db2util-1.0.6.beta-1.os400.noarch.rpm  repodata                               rsync-3.1.2-1.os400.noarch.rpm
$ createrepo ../RPMS/noarch/.
Spawning worker 0 with 2 pkgs
Workers Finished
Saving Primary metadata
Saving file lists metadata
Saving other metadata
Generating sqlite DBs
Sqlite DBs complete
```

5) yum list available rpms in repository
```
$ yum list available 
Config time: 0.027
Yum Version: 3.4.3

COMMAND: yum list available 
Installroot: /
Ext Commands:

   available
Setting up Package Sacks
os400_file                                                                                                                                                       | 2.9 kB  00:00:00     
os400_file/primary_db                                                                                                                                            | 2.1 kB  00:00:00     
pkgsack time: 0.102
Reading Local RPMDB
rpmdb time: 0.000
Available Packages
rsync.noarch                                                                             3.1.2-1                                                                              os400_file
```

step 5) yum install RPM from repository
```
$ yum install rsync.noarch
Config time: 0.027
Yum Version: 3.4.3
COMMAND: yum install rsync.noarch 
Installroot: /
Ext Commands:

   rsync.noarch
Setting up Package Sacks
os400_file                                                                                                                                                       | 2.9 kB  00:00:00     
os400_file/primary_db                                                                                                                                            | 2.1 kB  00:00:00     
pkgsack time: 0.136
Reading Local RPMDB
rpmdb time: 0.001
Setting up Install Process
Obs Init time: 0.000
Resolving Dependencies
--> Running transaction check
---> Package rsync.noarch 0:3.1.2-1 will be installed
Checking deps for rsync.noarch 0:3.1.2-1 - u
--> Finished Dependency Resolution
Dependency Process ending
Depsolve time: 0.111

Dependencies Resolved

========================================================================================================================================================================================
 Package                                   Arch                                       Version                                      Repository                                      Size
========================================================================================================================================================================================
Installing:
 rsync                                     noarch                                     3.1.2-1                                      os400_file                                     307 k

Transaction Summary
========================================================================================================================================================================================
Install       1 Package

Total download size: 307 k
Installed size: 881 k
Is this ok [y/N]: y
Downloading Packages:
Member: rsync.noarch 0:3.1.2-1 - u
Adding Package rsync-3.1.2-1.noarch in mode u
Running Transaction Check
Transaction Check time: 0.001
Running Transaction Test
Transaction Test Succeeded
Transaction Test time: 0.026
Running Transaction
  Installing : rsync-3.1.2-1.noarch                                                                                                                                                 1/1 
VerifyTransaction time: 0.194
Transaction time: 0.597

Installed:
  rsync.noarch 0:3.1.2-1                                                                                                                                                                

Complete!
$ rsync --version         
rsync  version 3.1.2  protocol version 31
Copyright (C) 1996-2015 by Andrew Tridgell, Wayne Davison, and others.
Web site: http://rsync.samba.org/
Capabilities:
    64-bit files, 32-bit inums, 32-bit timestamps, 64-bit long ints,
    socketpairs, hardlinks, symlinks, no IPv6, batchfiles, inplace,
    append, ACLs, no xattrs, iconv, no symtimes, no prealloc

rsync comes with ABSOLUTELY NO WARRANTY.  This is free software, and you
are welcome to redistribute it under certain conditions.  See the GNU
General Public Licence for details.
$
```

