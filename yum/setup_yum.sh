#!/bin/sh
#
# global
#

PATH=/opt/freeware/bin:/QOpenSys/usr/bin:/QOpenSys/usr/sbin
LIBPATH=""
export PATH
export LIBPATH
YUM_BUNDLE="yum_bundle_v1"
OS400_BUNDLE="os400_bundle_v1"

function package_fixup_yum {
  cdhere=$(pwd)
  echo "x            0 *** start IBM i compatible AIX toolbox changes *** "
  echo "x            0 cd $cdhere"
  cd $cdhere

  echo "x            0 *** start IBM i untar $OS400_BUNDLE *** "
  echo "x            0 tar -xf $OS400_BUNDLE.tar"
  tar -xf $OS400_BUNDLE.tar
  echo "x            0 *** end IBM i untar $OS400_BUNDLE *** "
  echo "x            0 cd $cdhere"
  cd $cdhere

  echo "x            0 *** start IBM i yum patchs *** "
  echo "x            0 cd $OS400_BUNDLE/yum-patch"
  cd $OS400_BUNDLE/yum-patch
  echo "x            0 *** allow non-root profile to use yum *** "
  echo "x            0 cp yumcommands.py /opt/freeware/share/yum-cli/yumcommands.py"
  cp yumcommands.py /opt/freeware/share/yum-cli/yumcommands.py
  echo "x            0 cp yumupd.py /opt/freeware/share/yum-cli/yumupd.py"
  cp yumupd.py /opt/freeware/share/yum-cli/yumupd.py
  echo "x            0 *** end IBM i yum patchs *** "
  echo "x            0 cd $cdhere"
  cd $cdhere

  echo "x            0 *** start IBM i yum conf *** "
  echo "x            0 cd $OS400_BUNDLE/yum-conf"
  cd $OS400_BUNDLE/yum-conf
  echo "x            0 cp yum.conf-os400-ifs /opt/freeware/etc/yum/yum.conf"
  cp yum.conf-os400-ifs /opt/freeware/etc/yum/yum.conf
  echo "x            0 touch /var/log/yum.log"
  touch /var/log/yum.log
  echo "x            0 cp repodata.tar /QOpenSys/opt/freeware/src/packages/RPMS/noarch/."
  cp repodata.tar /QOpenSys/opt/freeware/src/packages/RPMS/noarch/.
  echo "x            0 cd /QOpenSys/opt/freeware/src/packages/RPMS/noarch"
  cd /QOpenSys/opt/freeware/src/packages/RPMS/noarch
  echo "x            0 tar -xf repodata.tar"
  tar -xf repodata.tar
  echo "x            0 *** end IBM i yum conf *** "
  echo "x            0 cd $cdhere"
  cd $cdhere

  echo "x            0 *** start IBM i yum utilities *** "
  echo "x            0 cd $OS400_BUNDLE/yum-os400"
  cd $OS400_BUNDLE/yum-os400
  echo "x            0 cp os400repackage /opt/freeware/bin/."
  cp os400repackage /opt/freeware/bin/.
  echo "x            0 *** end IBM i yum utilities *** "
  echo "x            0 cd $cdhere"
  cd $cdhere

  echo "x            0 *** start fix-up IBM python2 (if needed) *** "
  if test -e /QOpenSys/QIBM/ProdData/OPS/Python2.7/bin/python2.7; then
    echo "x            0 cd /QOpenSys/usr/bin"
    cd /QOpenSys/usr/bin
    ibm_bins=""
    ibm_bins="$ibm_bins easy_install easy_install-2.7 pip pip2"
    ibm_bins="$ibm_bins pydoc"
    ibm_bins="$ibm_bins python python2 python2.7"
    ibm_bins="$ibm_bins python-config python2-config python2.7-config"
    for ibm_bin in $ibm_bins; do
      echo "x            0 ln -sf ../../QIBM/ProdData/OPS/Python2.7/bin/$ibm_bin $ibm_bin"
      ln -sf ../../QIBM/ProdData/OPS/Python2.7/bin/$ibm_bin $ibm_bin
    done
    echo "x            0 cd /QOpenSys/usr/lib"
    cd /QOpenSys/usr/lib
    ibm_libs=""
    ibm_libs="$ibm_libs libpython2.7.so"
    for ibm_lib in $ibm_libs; do
      echo "x            0 ln -sf ../../QIBM/ProdData/OPS/Python2.7/lib/$ibm_lib $ibm_lib"
      ln -sf ../../QIBM/ProdData/OPS/Python2.7/lib/$ibm_lib $ibm_lib
    done
  else
    echo "x            0 /QOpenSys/QIBM/ProdData/OPS/Python2.7/bin/python2.7 not found"
  fi
  echo "x            0 *** end fix-up IBM python2 (if needed) *** "
  echo "x            0 cd $cdhere"
  cd $cdhere


  echo "x            0 *** end IBM i compatible AIX toolbox changes *** "

  echo "x            0 *** end yum install *** "

}

#
# setup RPM (standard)
#
function package_setup_yum {
  cdhere=$(pwd)
  echo "setup $YUM_BUNDLE ..."
  echo "x            0 tar -xf $YUM_BUNDLE.tar"
  tar -xf $YUM_BUNDLE.tar

  echo "x            0 *** start yum install *** "
  echo "x            0 cd $YUM_BUNDLE/aix_toolbox_rpms"
  cd $YUM_BUNDLE/aix_toolbox_rpms
  echo "x            0 rpm -hiv --replacepkgs --noscripts *.rpm"
  rpm -hiv --replacepkgs --noscripts *.rpm
  echo "x            0 cd $cdhere"
  cd $cdhere
}

function package_test_yum {
  echo "**********************"
  echo "yum --version"
  echo "**********************"
  echo "PATH=$PATH"
  echo "LIBPATH=$LIBPATH"
  yum --version
}

#
# main
#
if [ -d /QOpenSys/usr/bin ]
then
  package_setup_yum
  package_fixup_yum
  package_test_yum
fi

